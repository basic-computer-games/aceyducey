# AceyDucey

A C# (.NET Core) conversion of the AceyDucey game from the 1978 book "BASIC Computer Games". Part of [codinghorror](https://twitter.com/codinghorror)'s [Updating “101 Basic Computer Games” for 2021](https://discourse.codinghorror.com/t/updating-101-basic-computer-games-for-2021/7927) project.

Converted to C# by Adam Dawes (@AdamDawes575, https://adamdawes.com).
